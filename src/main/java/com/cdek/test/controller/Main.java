package com.cdek.test.controller;

import com.cdek.test.model.User;
import com.cdek.test.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.ArrayList;
import java.util.List;

@RestController
public class Main extends WebMvcConfigurerAdapter {

	private final UserService userService;

	@Autowired
	public Main(UserService userService) {
		this.userService = userService;
	}

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addRedirectViewController("/", "/view");

		registry.addViewController("/view")
				.setViewName("view");
	}

	/**
	 * Вывод всех пользователей
	 * @param findId id пользователя для поиска
	 * @param findName имя пользователя для поиска
	 */
	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public ModelAndView view(
			@RequestParam(value = "findId", defaultValue = "0") Long findId,
			@RequestParam(value = "findName", defaultValue = "") String findName
	) {
		ModelMap modelMap = new ModelMap();

		if (findId == 0 && (findName == null || findName.equals(""))) {
			modelMap.put("users", userService.findAll());
		} else if (findId > 0) {
			List<User> users = new ArrayList<>();

			User findUser = userService.findById(findId);
			if (findUser != null) {
				users.add(findUser);
				modelMap.put("users", users);
			} else {
				modelMap.put("labelNotFound", "Пользователь с ID " + findId + " не найден");
			}
		} else if (!findName.equals("")) {
			List<User> users = new ArrayList<>(userService.findByName(findName));

			if (users.isEmpty()) {
				modelMap.put("labelNotFound", "Пользователи с именем " + findName + " не найдены");
			} else {
				modelMap.put("users", users);
			}
		}

		return new ModelAndView("view", modelMap);
	}

	/**
	 * Страница добавления новых пользователей
	 */
	@RequestMapping(value = "/adding", method = RequestMethod.GET)
	public ModelAndView adding() {
		return new ModelAndView("adding");
	}

	/**
	 * Добавление нового пользователя
	 * @param name имя нового пользователя
	 */
	@RequestMapping(value = "/adding", method = RequestMethod.POST)
	public ModelAndView addUser(
			@RequestParam("name") String name
	) {
		ModelMap modelMap = new ModelMap();

		if (!name.equals("")) {
			modelMap.put("user", userService.createUser(name));
			modelMap.put("labelAddUser", "Вы успешно добавили пользователя " + name);
		}

		return new ModelAndView("adding", modelMap);
	}
}
