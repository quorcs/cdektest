package com.cdek.test.dao;

import com.cdek.test.model.User;

import java.util.List;

public interface UserDAO {

	void createUser(String name);

	User getUserById(Long id);

	List<User> getAllUsers();

	List<User> getUsersByName(String name);
}
