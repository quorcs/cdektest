package com.cdek.test.dao.jdbc;

import com.cdek.test.dao.UserDAO;
import com.cdek.test.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.logging.Logger;

@Transactional
@Repository
public class UserDaoImpl implements UserDAO {

	private static Logger log = Logger.getLogger(UserDaoImpl.class.getName());

	private final JdbcTemplate jdbcTemplate;

	@Autowired
	public UserDaoImpl(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	/**
	 * Добавление нового пользователя в базу
	 * @param name имя нового пользователя
	 */
	@Override
	public void createUser(String name) {
		String sql = "INSERT INTO `users` SET `name` = ?;";

		try {
			jdbcTemplate.update(sql, name);
		} catch (EmptyResultDataAccessException e) {
			log.info("Пользователь не был создан: " + e);
		}
	}

	/**
	 * Поиск пользователя по id
	 * @param id id пользователя
	 */
	@Override
	public User getUserById(Long id) {
		String sql = "SELECT * FROM `users` WHERE `id` = ?;";

		try {
			return jdbcTemplate.queryForObject(sql, new Object[]{id}, new UserMapper());
		} catch (EmptyResultDataAccessException e) {
			log.info("Пользователь с id " + id + " не найден: " + e);
			return null;
		}
	}

	/**
	 * Получение всех пользователей
	 */
	@Override
	public List<User> getAllUsers() {
		String sql = "SELECT * FROM `users`;";

		try {
			return jdbcTemplate.query(sql, new UserMapper());
		} catch (EmptyResultDataAccessException e) {
			log.info("Запрос не вернул результатов: " + e);
			return null;
		}
	}

	/**
	 * Поиск пользователя по имени
	 * @param name имя пользователя
	 */
	@Override
	public List<User> getUsersByName(String name) {
		String sql = "SELECT * FROM `users` WHERE `name` LIKE ?;";

		try {
			return jdbcTemplate.query(sql, new Object[]{name}, new UserMapper());
		} catch (EmptyResultDataAccessException e) {
			log.info("Пользователи с именем " + name + " не найдены: " + e);
			return null;
		}
	}
}
