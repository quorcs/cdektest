package com.cdek.test.service;

import com.cdek.test.model.User;

import java.util.Collection;

public interface UserService {

	Collection<User> findAll();

	User findById(Long id);

	Collection<User> findByName(String name);

	User createUser(String name);
}
