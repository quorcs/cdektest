package com.cdek.test.service;

import com.cdek.test.dao.UserDAO;
import com.cdek.test.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class UserServiceImpl implements UserService {

	private final UserDAO userDAO;

	@Autowired
	public UserServiceImpl(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	@Override
	public Collection<User> findAll() {
		return userDAO.getAllUsers();
	}

	@Override
	public User findById(Long id) {
		return userDAO.getUserById(id);
	}

	@Override
	public Collection<User> findByName(String name) {
		return userDAO.getUsersByName(name);
	}

	/**
	 * Создание пользователя
	 * @param name имя нового пользователя
	 * @return User добавленный пользователь
	 */
	@Override
	public User createUser(String name) {

		userDAO.createUser(name);

		return findById((long) findAll().size());
	}
}
