<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Тестовое задание</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="resources/css/index.css">

    <div id="header">
        <div align="center">
            Добавление пользователя
        </div>
    </div>
</head>
<body>
<div id="body_block">
    <div>
        <table align="center">
            <form action="adding" method="post">
                <tr>
                    <td>
                        <input type="text" placeholder="Введите имя" name="name" />
                    </td>
                    <td>
                        <input type="submit" value="Добавить пользователя" />
                    </td>
                </tr>
            </form>
            <tr>
                <td colspan="2" align="center">
                    <label>${labelAddUser}</label>
                </td>
            </tr>
            <tr>
                <td>

                </td>
            </tr>
            <form action="view" method="get">
                <tr>
                    <td colspan="2" align="center">
                        <input type="submit" value="Показать всех пользователей" />
                    </td>
                </tr>
            </form>
        </table>
    </div>
</div>
</body>
</html>