<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Тестовое задание</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="resources/css/index.css">

    <div id="header">
        <div align="center">
            Просмотр данных
        </div>
    </div>
</head>
<body>
<div id="body_block">
    <div>
        <table align="center" border="1">
            <form action="view" method="get">
                <tr>
                    <td>
                        <input type="text" placeholder="Введите ID" name="findId" />
                    </td>
                    <td>
                        <input type="submit" value="Поиск по ID" />
                    </td>
                </tr>
            </form>
            <form action="view" method="get">
                <tr>
                    <td>
                        <input type="text" placeholder="Введите имя" name="findName" />
                    </td>
                    <td>
                        <input type="submit" value="Поиск по имени" />
                    </td>
                </tr>
            </form>
            <form action="view" method="get">
                <tr>
                    <td colspan="2" align="center">
                        <input type="submit" value="Показать всех" />
                    </td>
                </tr>
            </form>
            <form action="adding" method="get">
                <tr>
                    <td colspan="2" align="center">
                        <input type="submit" value="Добавить пользователя" />
                    </td>
                </tr>
            </form>
            <tr>
                <td colspan="2" align="center">
                    <label>${labelNotFound}</label>
                </td>
            </tr>
            <tr>
                <td align="center">
                    ID
                </td>
                <td align="center">
                    Name
                </td>
            </tr>
            <c:forEach var="user" items="${users}" >
                <tr>
                    <td>
                        <c:out value="${user.id}" />
                    </td>
                    <td>
                        <c:out value="${user.name}" />
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>
</div>
</body>
</html>