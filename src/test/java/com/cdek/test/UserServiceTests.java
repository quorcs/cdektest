package com.cdek.test;

import com.cdek.test.model.User;
import com.cdek.test.service.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTests {

	@Autowired
	private UserService userService;

	/**
	 * Тест для проверки добавления нового пользователя в базу
	 */
	@Test
	public void userAddTest() throws Exception {
		Assert.assertNotNull(userService.createUser("Test"));
		System.out.println("Adding user test been passed!");
	}

	/**
	 * Тест для проверки получение всех пользователей из базы
	 */
	@Test
	public void getAllUsersTest() {
		Assert.assertEquals(12, userService.findAll().size());
		System.out.println("Getting all users test been passed!");
	}

	/**
	 * Тест для проверки получения пользователя из базы по ID
	 */
	@Test
	public void getUserByIdTest() {
		User testUser = userService.findByName("Test").iterator().next();

		Assert.assertEquals(new User(12L, "Test"), testUser);

		System.out.println("Getting user by id test been passed!");
	}

	/**
	 * Тест для проверки получения пользователя из базы по имени
	 */
	@Test
	public void getUserByNameTest() {
		User testUser = userService.findByName("Ivan").iterator().next();

		Assert.assertEquals(new User(1L, "Ivan"), testUser);

		System.out.println("Getting user by name test been passed!");
	}

	/**
	 * Проверка на отсутствие в базе пользователя с ID 1000
	 */
	@Test
	public void notFoundIdTest() {
		Assert.assertNull(userService.findById(1000L));
		System.out.println("Not found user by ID test been passed!");
	}

	/**
	 * Проверка на отсутствие в базе пользователей с именем Evpatiy
	 */
	@Test
	public void notFoundNameTest() {
		Assert.assertFalse(userService.findByName("Evpatiy").iterator().hasNext());
		System.out.println("Not found user by name test been passed!");
	}
}
